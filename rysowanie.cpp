﻿/**
* \file rysowanie.cpp
* \brief Plik zawierajacy funkcje odpwoiadajace za rysowanie plansz
*/

#include <vector>
#include <iostream>
#include <algorithm>
#include "rysowanie.h"
using namespace std;
int alarm = true;


/** \brief Inicjalizacja_planszy
* Funkcja Inicjalizacja_planszy rysuje czysta plansze po raz pierwszy 
* \param rozmiar_tablicy okresla rozmiar tablicy
* \param plansza okresla plansze
* \return vector;
*/
vector<vector<char> > Inicjalizacja_planszy(int rozmiar_tablicy, vector<vector<char> > plansza)
{

	for (int i = 0; i < rozmiar_tablicy; i++)
		for (int j = 0; j < rozmiar_tablicy; j++)
			plansza[i][j] = ' ';

	int p = 1;
	for (int i = 2; i < rozmiar_tablicy; i++)
	{
		if (i % 2 == 0)
		{
			plansza[i][0] = p + 48;
			p++;
		}
	}

	p = 1;
	for (int i = 2; i < rozmiar_tablicy; i++)
	{
		if (i % 2 == 0)
		{
			plansza[0][i] = p + 64;
			p++;
		}
	}
	for (int i = 2; i < rozmiar_tablicy; i++)
		for (int j = 2; j < rozmiar_tablicy; j++)
		{
		if ((j % 2 == 0) && (i % 2 == 0))
			plansza[i][j] = 'O';
		else
			plansza[i][j] = ' ';

		}

	return plansza;
}
/** \brief Rysowanie_kresek
* Funkcja Rysowanie_kresek rysuje polaczenia miedzy punktami, sprawdzajac czy nie sa juz aby polaczone
* \param rozmiar_planszy okresla rozmiar planszy
* \param plansza okresla plansze
* \param i1 wspolrzedna pola1
* \param j1 wspolrzedna pola1
* \param i2 wspolrzedna pola2
* \param j2 wspolrzedna pola2
* \return vector;
*/
vector<vector<char> > Rysowanie_kresek(int i1, int i2, int j1, int j2, int rozmiar_planszy, vector<vector<char> > plansza)
{

	if (i1 == i2)
	{
		int posX = max(j1, j2) - 1;
		if (plansza[posX][i1] == '|')
			alarm = false;
		else
		{
			alarm = true;
			plansza[posX][i1] = '|';
		}
	}
	else if (j1 == j2)
	{
		int posY = max(i1, i2) - 1;
		if (plansza[j1][posY] == '-')
			alarm = false;
		else
		{
			alarm = true;
			plansza[j1][posY] = '-';
		}
	}
	
	return plansza;
}
/** \brief Rysowanie_planszy
* Funkcja Rysowanie_planszy rysuje plansze z zaznaczeniem już wykonanych ruchow
* \param rozmiar_planszy okresla rozmiar planszy
* \param plansza okresla plansze
* \param liczba_ruchow okresla liczbe wykonanych juz ruchow
*/
void Rysowanie_planszy(vector<vector<char> > plansza, int rozmiar_planszy, int liczba_ruchow)
{
	cout << endl;
	cout << "Plansza: " << endl;
	cout << endl;

	for (int i = 0; i < rozmiar_planszy; i++){

		for (int j = 0; j < rozmiar_planszy; j++)
			cout << plansza[i][j];
		cout << endl;
	}
	cout << endl;
	cout << "Liczba wykonanych ruchów: " << liczba_ruchow << endl;
	cout << endl;
}
