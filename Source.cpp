/**
* \mainpage
* \par Gra "Dots and boxes"
* \author Tomasz �arek
* \date 2015.08.31.
* \file Source.cpp
* \brief Plik zrodlowy zawierajacy funkcje main()
*/

#include <iostream>
#include <Windows.h> // do czyszczenia ekranu
#include <string>
#include <cstdlib> // funckja atoi 
#include <ctime>
#include <vector>
#include <conio.h> // do polskich znakow 
#include "rysowanie.h"
#include "obsluga_graczy.h"
#include "Algorytmik.h"
using namespace std;
extern int alarm;
int Menu_Glowne();
void Zasady();
void Zwloka(float sekund);
void Gra_2G();
void Gra_GK();
int main(){
	setlocale(LC_ALL, ""); // do polskich znakow

	cout << "Gra \"Szewc\"             \nImie i Nazwisko: Gabriela Kowalik\nKierunek: Teleinformatyka" << endl;
	system("PAUSE"); 
	int wybor;
	while (1)
	{
		wybor = Menu_Glowne();
		switch(wybor)
		{
		case 1: 
			Gra_2G();
			break;
		case 2:
			Gra_GK();
			break;
		case 3: 
			Zasady();
			break;
		default:
			cout << "Do widzenia!";
			Zwloka(2);
			return 0;
		}
	}
	
	system("PAUSE");
}
/** \brief Menu_Glowne
* Funkcja Menu_Glowne wyswietla menu
* \return INTEGER;
*/
int  Menu_Glowne(){
	char krecha[] = "               ##################################################               ";
	char MenuGlowne[] = "               ################## MENU GLOWNE ###################               ";
	char ListaOpcji[] = "               ##                 LISTA OPCJI                  ##               ";
	char boki_sp[] = "               ##                                              ##               ";
	char boki[] = "               ##                                              ##\r               ##     ";
	string wybor_M_G;
	while (1)
	{
		system("CLS");
		cout << "\n\n\n\n" << krecha << MenuGlowne << krecha << ListaOpcji
			<< boki << "1. Gracz vs Gracz\n"
			<< boki << "2. Gracz vs Komputer\n"
			<< boki << "3. Zasady\n"
			<< boki << "4. Wyj�cie\n"
			<< boki_sp << krecha
			<< "\t\t\tWybierz opcje (1-4): ";

		//zapetlam w razie nieprawidlowego wprowadzenia
		cin >> wybor_M_G; //wybor funkcji w menu
		if (wybor_M_G == "1" || wybor_M_G == "2" || wybor_M_G == "3" || wybor_M_G == "4")
		{
			return atoi(wybor_M_G.data());
		}
		else
		{
			cout << "\t\tWprowadzona liczba jest niepoprawna..";
			Zwloka(0.5);
			cout << ".";
			Zwloka(2);
		}
	}
}
/** \brief Zasady
* Funkcja Zasady wyswietla zasady gry
*/
void Zasady()
{
	system("CLS");
	cout << "Gr� rozpoczyna si� z pust� plansz� (rozm: 3-9).\nGracze na przemian stawiaj� kreski na kraw�dziach kratek, "
		<< "ktore w programie \nprzedstawione s� jako znak 'O'. \nGracz, kt�ry jako ostatni wykona ruch zamykaj�cy obszar pomi�dzy znakami 'O'"
		<< "\nprzejmuje go jako swoje terytorium i oznaczone ono jest znakiem wybranym na \npocz�tku gry. Gra toczy si� do "
		<< "zacieniowania ostatniej wolnej kratki. \nGracz, kt�rego terytorium jest wi�ksze, wygrywa."
		<< "\nZwykle w kreski gra si� w dwie osoby, w grze mo�e jednak uczestniczy� wi�ksza \nliczba graczy.\n\n"
		<< "�r�d�o: pl.wikipedia.org/wiki/Szewc_(gra) \n"<<endl;
	cout << "Kliknij (Q) aby wr�ci� do menu";
	Zwloka(1);
	cout << endl;
	string wybor_Z;
	while (1)
	{
		cin >> wybor_Z;
		if (wybor_Z == "q" || wybor_Z=="Q") break;
		wybor_Z = ""; // nie wiem czy potrzebne
	}
}

/** \brief Zwloka
* Funkcja Zwloka reguluje czas zmiany ekranu
* \param sekund reprezentuje liczbe sekund po jakich ma sie dokonac zmiana ekranu
*/

void Zwloka(float sekund)
{
	int poczatkowy_czas = time(NULL);
	while (time(NULL) - poczatkowy_czas < sekund); // petla wykonujaca sie do uplyni�cia liczby 'sekund'
}
/** \brief Gra
* Funkcja Gra wywoluje wlasciwa gre
*/
void Gra_2G()
{

	system("CLS");
	cout << endl;
	cout << "Zaczynajmy!" << endl;
	cout << endl;
	
	char znakGracza1, znakGracza2;

	do
	{
		znakGracza1 = wybor_znaku(1);
		znakGracza2 = wybor_znaku(2);
		system("CLS");
	} while (!(znaki_Graczy(znakGracza1, znakGracza2)));

	int rozmiar = 0;
	int polaGracza1 = 0;
	int polaGracza2 = 0;

	rozmiar = Rozmiar_planszy(rozmiar);

	int rozmiar_planszy = rozmiar * 2 + 1;
	vector < vector <char> > plansza;
	plansza.resize(rozmiar_planszy);
	for (int i = 0; i < plansza.size(); i++)
	plansza[i].resize(rozmiar_planszy);
	

	plansza = Inicjalizacja_planszy(rozmiar_planszy, plansza);
	string pole1, pole2;
	char literka1, literka2;
	int cyferka1, cyferka2;
	int gracz = 1;
	int liczba_ruchow = 0;

	
	system("CLS");

	Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

	Algorytmik nowa_gra(false, false, false, false, false, false, false);

	do
	{
		nowa_gra.ostatni_ruch = false;
		cout << "Graczu nr " << gracz << ", wybierz kropk�: " << endl;
		cin >> pole1;
		while (pole1.length() != 2)
		{
			system("CLS");

			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

			cout << "Wprowadzenie jest z�ej d�ugo�ci! Wprowad� ponownie:" << endl;
			cout << "Graczu nr " << gracz << ", wybierz kropk�: " << endl;
			cin.clear();
			cin >> pole1;
		}
		while ((!(Poprawnosc_litery(pole1[0], rozmiar))) || (!(Poprawnosc_liczby(pole1[1], rozmiar))))
		{
			system("CLS");

			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

			cout << "Wykryto nieprawid�owe oznaczenie pola!. Wprowad� dane jeszcze raz:" << endl;
			cout << "Graczu nr " << gracz << ", wybierz kropk�: " << endl;
			cin.clear();
			cin >> pole1;
		}

		literka1 = Poprawnosc_litery(pole1[0], rozmiar);
		cyferka1 = Poprawnosc_liczby(pole1[1], rozmiar);

		cout << "Graczu nr " << gracz << ", z kt�r� kropk� chcesz po��czy� " << pole1[0] << pole1[1] << " ?" << endl;
		cin >> pole2;
		while (pole2.length() != 2)
		{
			system("CLS");

			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

			cout << "Wprowadzenie jest z�ej d�ugo�ci! Wprowad� ponownie:" << endl;
			cout << "Graczu nr " << gracz << ", z kt�r� kropk� chcesz po��czy� " << pole1[0] << pole1[1] << " ?" << endl;
			cin.clear();
			cin >> pole2;
		}
		while ((!(Poprawnosc_litery(pole2[0], rozmiar))) || (!(Poprawnosc_liczby(pole2[1], rozmiar))))
		{
			system("CLS");
			
			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);
				
			cout << "Wykryto nieprawid�owe oznaczenie pola!. Wprowad� dane jeszcze raz:" << endl;
			cout << "Graczu nr " << gracz << ", z kt�r� kropk� chcesz po��czy� " << pole1[0] << pole1[1] << " ?" << endl;
			cin.clear();
			cin >> pole2;
		}

		literka2 = Poprawnosc_litery(pole2[0], rozmiar);
		cyferka2 = Poprawnosc_liczby(pole2[1], rozmiar);

		if (!(nowa_gra.czy_sasiednie(literka1, literka2, cyferka1, cyferka2)))
		{
			system("CLS");
			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);
			cout << "Kropki nie s� swoimi s�siadami. Wprowad� kropki jeszcze raz!" << endl;
		}
		else
		{
			plansza = Rysowanie_kresek(literka1, literka2, cyferka1, cyferka2, rozmiar_planszy, plansza);

			if (!(alarm))
			{
				system("CLS");
				Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);
				cout << "Kropki s� ju� po��czone! Wprowad� inne:" << endl;
			}
			else
			{	
				system("CLS");

				char znakGracza = ktory_znak(gracz, znakGracza1, znakGracza2);
				plansza = nowa_gra.sprawdz_pole(plansza, rozmiar_planszy, znakGracza);
				liczba_ruchow++;
				Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);
				if ((nowa_gra.ostatni_ruch) && (liczba_ruchow < (rozmiar*(rozmiar * 2 - 2))))
				cout << "Zdoby�e� pole! Masz dodatkowy ruch:" << endl;
				else if (liczba_ruchow < (rozmiar*(rozmiar * 2 - 2)))
				gracz = zmiana_Graczy(gracz);

			}

		}

	} 
	while (liczba_ruchow < (rozmiar*(rozmiar*2 - 2)));

	cout << "Koniec gry!" << endl;

	Zwloka(3);

	system("CLS");

	Wyniki(plansza, rozmiar_planszy, znakGracza1, znakGracza2, polaGracza1, polaGracza2);

	Zwloka(5);
}
void Gra_GK()
{
	srand(time(NULL));

	system("CLS");
	cout << endl;
	cout << "Zaczynajmy!" << endl;
	cout << endl;

	char znakGracza;
	char znakKomputera = 'K';
	
	znakGracza = wybor_znaku(0);
	system("CLS");

	int rozmiar = 0;
	int polaGracza = 0;
	int polaKomputera = 0;

	rozmiar = Rozmiar_planszy(rozmiar);

	int rozmiar_planszy = rozmiar * 2 + 1;
	vector < vector <char> > plansza;
	plansza.resize(rozmiar_planszy);
	for (int i = 0; i < plansza.size(); i++)
		plansza[i].resize(rozmiar_planszy);


	plansza = Inicjalizacja_planszy(rozmiar_planszy, plansza);
	string pole1, pole2;
	char literka1, literka2;
	int cyferka1, cyferka2;
	int liczba_ruchow = 0;


	system("CLS");

	Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

	Algorytmik nowa_gra(false, false, false, false, false, false,false);

	do
	{
		nowa_gra.ostatni_ruch = false;

		cout << "Graczu, wybierz kropk�: " << endl;
		cin >> pole1;
		while (pole1.length() != 2)
		{
			system("CLS");

			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

			cout << "Wprowadzenie jest z�ej d�ugo�ci! Wprowad� ponownie:" << endl;
			cout << "Graczu, wybierz kropk�: " << endl;
			cin.clear();
			cin >> pole1;
		}
		while ((!(Poprawnosc_litery(pole1[0], rozmiar))) || (!(Poprawnosc_liczby(pole1[1], rozmiar))))
		{
			system("CLS");

			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

			cout << "Wykryto nieprawid�owe oznaczenie pola!. Wprowad� dane jeszcze raz:" << endl;
			cout << "Graczu, wybierz kropk�: " << endl;
			cin.clear();
			cin >> pole1;
		}

		literka1 = Poprawnosc_litery(pole1[0], rozmiar);
		cyferka1 = Poprawnosc_liczby(pole1[1], rozmiar);

		cout << "Graczu, z kt�r� kropk� chcesz po��czy� " << pole1[0] << pole1[1] << " ?" << endl;
		cin >> pole2;
		while (pole2.length() != 2)
		{
			system("CLS");

			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

			cout << "Wprowadzenie jest z�ej d�ugo�ci! Wprowad� ponownie:" << endl;
			cout << "Graczu, z kt�r� kropk� chcesz po��czy� " << pole1[0] << pole1[1] << " ?" << endl;
			cin.clear();
			cin >> pole2;
		}
		while ((!(Poprawnosc_litery(pole2[0], rozmiar))) || (!(Poprawnosc_liczby(pole2[1], rozmiar))))
		{
			system("CLS");

			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

			cout << "Wykryto nieprawid�owe oznaczenie pola!. Wprowad� dane jeszcze raz:" << endl;
			cout << "Graczu, z kt�r� kropk� chcesz po��czy� " << pole1[0] << pole1[1] << " ?" << endl;
			cin.clear();
			cin >> pole2;
		}

		literka2 = Poprawnosc_litery(pole2[0], rozmiar);
		cyferka2 = Poprawnosc_liczby(pole2[1], rozmiar);

		if (!(nowa_gra.czy_sasiednie(literka1, literka2, cyferka1, cyferka2)))
		{
			system("CLS");
			Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);
			cout << "Kropki nie s� swoimi s�siadami. Wprowad� kropki jeszcze raz!" << endl;
		}
		else
		{
			plansza = Rysowanie_kresek(literka1, literka2, cyferka1, cyferka2, rozmiar_planszy, plansza);

			if (!(alarm))
			{
				system("CLS");
				Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);
				cout << "Kropki s� ju� po��czone! Wprowad� inne:" << endl;
			}
			else
			{
				system("CLS");
		
				plansza = nowa_gra.sprawdz_pole(plansza, rozmiar_planszy, znakGracza);
				liczba_ruchow++;
				Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);

				if ((nowa_gra.ostatni_ruch) && (liczba_ruchow < (rozmiar*(rozmiar * 2 - 2))))
					cout << "Zdoby�e� pole! Masz dodatkowy ruch:" << endl;
				else if  (liczba_ruchow < (rozmiar*(rozmiar * 2 - 2)))
				{	
					
					do
					{
						nowa_gra.ostatni_ruch = false;
						cout << "Teraz ruch komputera..." << endl;
						int pozycjaX;
						int pozycjaY;
						do
						{
							
							pozycjaX = rand() % (rozmiar_planszy - 2) + 2;
							pozycjaY = rand() % (rozmiar_planszy - 2) + 2;

							if ((plansza[pozycjaX][pozycjaY] == ' '))
							{
								if ((pozycjaX % 2 == 0) && (pozycjaY % 2 == 1))
								{
									plansza[pozycjaX][pozycjaY] = '-';
									break;
								}
								else if ((pozycjaX % 2 == 1) && (pozycjaY % 2 == 0))
								{
									plansza[pozycjaX][pozycjaY] = '|';
									break;
								}
							}
				

						} while (1);
						liczba_ruchow++;
						plansza = nowa_gra.sprawdz_pole(plansza, rozmiar_planszy, znakKomputera);
						Zwloka(1);
						system("CLS");
						Rysowanie_planszy(plansza, rozmiar_planszy, liczba_ruchow);
						if ((nowa_gra.ostatni_ruch) && (liczba_ruchow < (rozmiar*(rozmiar * 2 - 2))))
						{
							cout << "Komputer zdoby� pole i ma dodatkowy ruch!" << endl;
							Zwloka(1);
						}
						
					}
					while ((nowa_gra.ostatni_ruch) && (liczba_ruchow < (rozmiar*(rozmiar * 2 - 2))));
				}
			}

		}

	} while (liczba_ruchow < (rozmiar*(rozmiar * 2 - 2)));

	cout << "Koniec gry!" << endl;

	Zwloka(3);

	system("CLS");

	Wyniki(plansza, rozmiar_planszy, znakGracza, 'K', polaGracza, polaKomputera);

	Zwloka(5);
}