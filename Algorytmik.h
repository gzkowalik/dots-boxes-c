#pragma once

#include <vector>
#include <iostream>
using namespace std;
class Algorytmik
{
public:
	/// logiczny sk�adnik klasy mowiacy o tym, czy wykonalismy ruch w lewo
	bool wLewo;
	/// logiczny sk�adnik klasy mowiacy o tym, czy wykonalismy ruch w prawo
	bool wPrawo;
	/// logiczny sk�adnik klasy mowiacy o tym, czy wykonalismy ruch w gore
	bool wGore;
	/// logiczny sk�adnik klasy mowiacy o tym, czy wykonalismy ruch w dol
	bool wDol;
	/// logiczny sk�adnik klasy mowiacy o tym jezeli poszlismy w gore lub w dol to nie mozemy pojsc w odwrotna strone
	bool blokada_pionowo;
	/// logiczny sk�adnik klasy mowiacy o tym jezeli poszlismy w lewo lub w prawo to nie mozemy pojsc w odwrotna strone
	bool blokada_poziomo;
	/// zawiera wspolrzedne X kwadratu ktory bedzie zakreslal pole
	int pozycjeX[4];
	/// zawiera wspolrzedne Y kwadratu ktory bedzie zakreslal pole
	int pozycjeY[4];
	/// zmienne parametryczne do funkcji 
	int i1, i2, j1, j2, posX, posY, rozmiar_planszy;
	/// zmmienna przechowujaca reprezentacje literow� pola konkretnego gracza
	char znakGracza;
	/// kontener przechowujacy plansze A-...- 1-... 
	vector<vector<char> > plansza;
	bool ostatni_ruch;
	/// konstruktor parametryczny 
	Algorytmik(bool wLewo, bool wPrawo, bool wGore, bool wDol, bool blokada_pionowo, bool blokada_poziomo, bool ostatni_ruch);
	/// destruktor
	~Algorytmik();
	/// metoda sprawdzajaca czy parametry wspolrzedne punktow (i1,j1) oraz (i2,j2) sa sasiednie
	bool czy_sasiednie(int i1, int i2, int j1, int j2);
	/// metoda sprawdzajaca pole o zadanych paramterach, zwracajaca wektor reprezentujacy plansze
	vector<vector<char> > sprawdz_pole(vector<vector<char> > plansza, int rozmiar_planszy, char znakGracza);
	/// funkcja przyjmujaca kontener zawierajacy plansze, pozycje X,Y o sprawdza czy moze zostac wykonany ruch do gory
	int idzDoGory(vector<vector<char> > plansza, int posX, int posY);
	/// funkcja przyjmujaca kontener zawierajacy plansze, pozycje X,Y o sprawdza czy moze zostac wykonany ruch w prawo
	int idzWPrawo(vector<vector<char> > plansza, int posX, int posY, int rozmiar_planszy);
	/// funkcja przyjmujaca kontener zawierajacy plansze, pozycje X,Y o sprawdza czy moze zostac wykonany ruch w dol
	int idzWDol(vector<vector<char> > plansza, int posX, int posY, int rozmiar_planszy);
	/// funkcja przyjmujaca kontener zawierajacy plansze, pozycje X,Y o sprawdza czy moze zostac wykonany ruch w lewo
	int idzWLewo(vector<vector<char> > plansza, int posX, int posY);
};

