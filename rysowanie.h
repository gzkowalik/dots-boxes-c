/**
* \file rysowanie.h
* \brief Plik naglowkowy z deklaracjami funkcji do pliku rysowanie.cpp
*/

#include <vector>
#include <iostream>
using namespace std;

vector<vector<char> > Inicjalizacja_planszy(int rozmiar_tablicy, vector<vector<char> > plansza);
void Rysowanie_planszy(vector<vector<char> > plansza, int rozmiar_planszy, int liczba_ruchow);
vector<vector<char> > Rysowanie_kresek(int i1, int i2, int j1, int j2, int rozmiar_planszy, vector<vector<char> > plansza);
