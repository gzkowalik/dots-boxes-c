/**
* \file Algorytmik.cpp
* \brief Pkik zawierajacy klase odpowiadająca za algorytm gry
*/

#include "Algorytmik.h"
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

Algorytmik::Algorytmik(bool wLewo, bool wPrawo, bool wGore, bool wDol, bool blokada_pionowo, bool blokada_poziomo, bool ostatni_ruch)
{
	this->wLewo = wLewo;
	this->wPrawo = wPrawo;
	this->wGore = wGore;
	this->wDol = wDol;
	this->blokada_pionowo = blokada_pionowo;
	this->blokada_poziomo = blokada_poziomo;
	this->ostatni_ruch = ostatni_ruch;
}



Algorytmik::~Algorytmik()
{
}

bool Algorytmik::czy_sasiednie(int i1, int i2, int j1, int j2)
{

	if (((i1 + 2 == i2) || (i1 - 2 == i2)) && (j1 == j2))
		return true;
	else if (((j1 + 2 == j2) || (j1 - 2 == j2)) && (i1 == i2))
		return true;
	else
		return false;

}


int Algorytmik::idzWDol(vector<vector<char> > plansza, int posX, int posY, int rozmiar_planszy)
{

	if (posX + 1 < rozmiar_planszy)
	{
		if (plansza[posX + 1][posY] == '|')
		{
			pozycjeX[0] = posX;
			pozycjeY[0] = posY;
			posX += 2;
			wDol = true;
			blokada_pionowo = true;
			blokada_poziomo = false;
		}

	}
	return posX;

}

int Algorytmik:: idzDoGory(vector<vector<char> > plansza, int posX, int posY)
{
	if (plansza[posX - 1][posY] == '|')
	{
		pozycjeX[1] = posX;
		pozycjeY[1] = posY;
		posX -= 2;
		wGore = true;
		blokada_pionowo = true;
		blokada_poziomo = false;
	}

	return posX;
}
int Algorytmik:: idzWLewo(vector<vector<char> > plansza, int posX, int posY)
{

	if (plansza[posX][posY - 1] == '-')
	{
		pozycjeX[2] = posX;
		pozycjeY[2] = posY;
		wLewo = true;
		blokada_pionowo = false;
		blokada_poziomo = true;
		posY -= 2;
	}

	return posY;

}
int Algorytmik::idzWPrawo(vector<vector<char> > plansza, int posX, int posY, int rozmiar_planszy)
{
	if (posY + 1 < rozmiar_planszy)
	{
		if (plansza[posX][posY + 1] == '-')
		{
			pozycjeX[3] = posX;
			pozycjeY[3] = posY;
			wPrawo = true;
			blokada_pionowo = false;
			blokada_poziomo = true;
			posY += 2;

		}
	}

	return posY;
}


vector<vector<char> > Algorytmik::sprawdz_pole(vector<vector<char> > plansza, int rozmiar_planszy, char znakGracza)
{
	int licznik_ruchow = 0;
	for (int i = 2; i < rozmiar_planszy; i += 2)
	for (int j = 2; j < rozmiar_planszy; j += 2){

		int poczatkoweX = j;
		int poczatkoweY = i;
		int posX = j;
		int posY = i;

		do
		{

			if ((!wLewo) && (!blokada_poziomo))
				posY = idzWLewo(plansza, posX, posY);
			if ((!wGore) && (!blokada_pionowo))
				posX = idzDoGory(plansza, posX, posY);
			if ((!wPrawo) && (!blokada_poziomo))
				posY = idzWPrawo(plansza, posX, posY, rozmiar_planszy);
			if ((!wDol) && (!blokada_pionowo))
				posX = idzWDol(plansza, posX, posY, rozmiar_planszy);
			licznik_ruchow++;


		} while ((licznik_ruchow < 4) && ((wDol) || (wGore) || (wPrawo) || (wLewo)));

		if ((wDol) && (wGore) && (wPrawo) && (wLewo))
		{
			sort(pozycjeX, pozycjeX + 4);
			sort(pozycjeY, pozycjeY + 4);

			int znakX = (pozycjeX[1] + pozycjeX[2]) / 2;
			int znakY = (pozycjeY[1] + pozycjeY[2]) / 2;

			if (plansza[znakX][znakY] == ' ')
			{
				plansza[znakX][znakY] = znakGracza;
				ostatni_ruch = true;
			}
		}

		wLewo = false;
		wPrawo = false;
		wGore = false;
		wDol = false;
		blokada_pionowo = false;
		blokada_poziomo = false;
		licznik_ruchow = 0;
	}
	return plansza;
}
