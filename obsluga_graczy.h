/**
* \file obsluga_graczy.h
* \brief Plik naglowkowy z deklaracjami funkcji do pliku obsluga_graczy.cpp
*/
#include <vector>
#include <iostream>
using namespace std;

int Rozmiar_planszy(char rozmiar);
char wybor_znaku(int ktory_Gracz);
int Poprawnosc_litery(char litera, int rozmiar_planszy);
int Poprawnosc_liczby(char liczba, int rozmiar_planszy);
int zmiana_Graczy(int ktory_Gracz);
bool znaki_Graczy(char znak1, char znak2);
char ktory_znak(int ktory_Gracz, char znakGracza1, char znakGracza2);
void Wyniki(vector<vector<char> > plansza, int rozmiar_planszy, char znakGracza1, char znakGracza2, int polaGracza1, int polaGracza2);