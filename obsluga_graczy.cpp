/**
* \file obsluga_graczy.cpp
* \brief Plik zawierajacy funkcje obslugujace graczy
*/


#include <iostream>
#include <vector>
#include <Windows.h>
using namespace std;

/** \brief Rozmiar planszy
* Funkcja Rozmiar_planszy sprawdza czy jest dobre wprowadzenie przez uztykwonika rozmiaru planszy
* \param rozmiar przyjmuje liczbe z zakresu 3-9 jesli jest O.K
* \return INTEGER
*/
int Rozmiar_planszy(char rozmiar)
{
	do
	{
		cout << "\nWprowad� prawid�owy rozmiar planszy (3-9):" << endl;
		cin >> rozmiar;
		switch (rozmiar)
		{
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		default: system("CLS"); cout << " \nWprowadzono nieprawid�owe dane! Wprowad� je jeszcze raz: " << endl;
		}
	} while (1);

	system("CLS");
}

/** \brief Poprawnosc_litery
* Funkcja Poprawnosc_litery sprawdza czy jest dobre wprowadzenie przez uzytkownika litery w zaleznosci od rozmiaru planszy
* \param litera przyjmuje litere z zakresu A-I jesli jest O.K
* \return INTEGER
*/

int Poprawnosc_litery(char litera, int rozmiar_planszy)
{
	char znaczek;
	switch (litera)
	{
		case 'A': znaczek = 'A'; break;
		case 'B': znaczek = 'B'; break;
		case 'C': znaczek = 'C'; break;
		case 'D': znaczek = 'D'; break;
		case 'E': znaczek = 'E'; break;
		case 'F': znaczek = 'F'; break;
		case 'G': znaczek = 'G'; break;
		case 'H': znaczek = 'H'; break;
		case 'I': znaczek = 'I'; break;
		default: return 0;
	}

	int i;
	for (int m = 0; m < rozmiar_planszy; m++)
	{
		if (znaczek == 65 + m)
		{
			i = znaczek - (63 - m);
			return i;
		}
	}
	return 0;
}
/** \brief Poprawnosc_liczby
* Funkcja Poprawnosc_liczby sprawdza czy jest dobre wprowadzenie przez uzytkownika liczby w zaleznosci od rozmiaru planszy
* \param liczba przyjmuje liczba z zakresu 1-9 jesli jest O.K
* \return INTEGER
*/
int Poprawnosc_liczby(char liczba, int rozmiar_planszy)
{
	int cyferka;
	switch (liczba)
	{
	case '1': cyferka = 1; break;
	case '2': cyferka = 2; break;
	case '3': cyferka = 3; break;
	case '4': cyferka = 4; break;
	case '5': cyferka = 5; break;
	case '6': cyferka = 6; break;
	case '7': cyferka = 7; break;
	case '8': cyferka = 8; break;
	case '9': cyferka = 9; break;
	default: return 0;
	}
	int j;

	if (cyferka > rozmiar_planszy)
		return 0;
	else
		j = cyferka * 2;
	return j;
}
/** \brief wybor_znaku
* Funkcja wybor_znaku sprawdza czy jest dobre wprowadzenie przez uzytkownika znaku, ktorym ma okreslac zajete przez siebie pole, z wylaczaniem znakow "okraglych"
* \param ktory_Gracz okresla numer gracza
* \return CHAR
*/
char wybor_znaku(int ktory_Gracz)
{
	char oznaczenie;
	do
	{ 
		if (ktory_Gracz != 0)
		cout << "Graczu nr " << ktory_Gracz << " podaj znak, kt�rym chcesz oznacza� swoje pola: " << endl;
		else
		cout << "Graczu, podaj znak, kt�rym chcesz oznacza� swoje pola: " << endl;

		cin >> oznaczenie;
		if ((oznaczenie == 'o') || (oznaczenie == '0') || (oznaczenie == 'O') || (oznaczenie == 'K') || (oznaczenie == 'k'))
		{
			system("CLS");
			cout << "\nWprowad� inny znak! \n" << endl;
			oznaczenie = NULL;
		}
	} 
	while (oznaczenie == NULL);
	return oznaczenie;

}
/** \brief znaki_Graczy
* Funkcja znaki_Graczy sprawdza czy wybrane znaki graczy nie s� takie same
* \param znak1 reprezentuje znak gracza1
* \param znak2 reprezentuje znak gracza2
* \return bool
*/
bool znaki_Graczy(char znak1, char znak2)
{
	if (znak1 == znak2)
	{
		cout << "\nZnaki s� takie same! Wprowad� znaki jeszcze raz: \n" << endl;
		return false;
	}
	return true;
}
/** \brief zmiana_Graczy
* Funkcja zmiana_Graczy dokonuje zamiany numeru gracza
* \param ktory_Gracz reprezentuje numer bie��cego gracza
* \return INTEGER;
*/
int zmiana_Graczy(int ktory_Gracz)
{
	if (ktory_Gracz == 1)
		ktory_Gracz++;
	else if (ktory_Gracz == 2)
		ktory_Gracz--;
	return ktory_Gracz;
}
/** \brief ktory_znak
* Funkcja ktory_znak zwraca znak biezacego gracza
* \param ktory_Gracz reprezentuje numer bie��cego gracza
* \param znakGracza1 reprezentuje znak gracza1
* \param znakGracza2 reprezentuje znak gracza2
* \return INTEGER;
*/
char ktory_znak(int ktory_Gracz, char znakGracza1, char znakGracza2)
{
	if (ktory_Gracz == 1)
		return znakGracza1;
	else if (ktory_Gracz == 2)
		return znakGracza2;
}
/** \brief Wyniki
* Funkcja Wyniki okresla ktory z graczy wygral gre
* \param plansza reprezentuje plansze
* \param rozmiar_planszy reprezentuje rozmiar planszy
* \param znakGracza1 jest znakiem gracza1
* \param znakGracza2 jest znakiem gracza2
* \param polaGracza1 okresla ile pol jest zajetych przez gracza1
* \param polaGracza2 okresla ile pol jest zajetych przez gracza2
*/
void Wyniki(vector<vector<char> > plansza, int rozmiar_planszy, char znakGracza1, char znakGracza2, int polaGracza1, int polaGracza2)
{
	for (int i = 3; i < rozmiar_planszy; i += 2)
		for (int j = 3; j < rozmiar_planszy; j += 2)
		{
			if (plansza[i][j] == znakGracza1)
			polaGracza1++;
			else if (plansza[i][j] == znakGracza2)
			polaGracza2++;
		}
	cout << endl;
	cout << "Wyniki: " << endl;
	cout << endl;
	if (znakGracza2 == 'K')
	{
		cout << "Pola zdobyte przez gracza : " << polaGracza1 << endl;
		cout << "Pola zdobyte przez komputer: " << polaGracza2 << endl;
		cout << endl;

		if (polaGracza1 > polaGracza2)
			cout << "Wygrywa gracz !" << endl;
		else if (polaGracza1 < polaGracza2)
			cout << "Wygrywa komputer!" << endl;
		else
			cout << "Remis!" << endl;

		cout << endl;

	}
	else
	{
		cout << "Pola zdobyte przez gracza nr 1 : " << polaGracza1 << endl;
		cout << "Pola zdobyte przez gracza nr 2 : " << polaGracza2 << endl;
		cout << endl;

		if (polaGracza1 > polaGracza2)
			cout << "Wygrywa gracz 1!" << endl;
		else if (polaGracza1 < polaGracza2)
			cout << "Wygrywa gracz 2!" << endl;
		else
			cout << "Remis!" << endl;

		cout << endl;
	}

	cout << "Wracamy do menu..." << endl;
}